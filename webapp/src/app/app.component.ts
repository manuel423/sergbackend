import { Component } from '@angular/core';

@Component({
  selector: 'serg-root',
  template: '<router-outlet></router-outlet>',
})
export class AppComponent {
  title = 'serg';
}
