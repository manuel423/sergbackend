import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { PreviousWorkComponent } from './previous-work/previous-work.component';
import { PricingComponent } from './pricing/pricing.component';
import { ProfileComponent } from './profile/profile.component';
import { ReservationsComponent } from './reservations/reservations.component';

const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
  },
  {
    path: 'prevworks',
    component: PreviousWorkComponent,
  },
  {
    path: 'pricing',
    component: PricingComponent,
  },
  {
    path: 'profile',
    component: ProfileComponent,
  },
  {
    path: 'reservations',
    component: ReservationsComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class VendorRoutingModule {}
