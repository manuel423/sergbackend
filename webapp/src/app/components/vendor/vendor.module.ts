import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VendorRoutingModule } from './vendor-routing.module';
import { ReservationsComponent } from './reservations/reservations.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ProfileComponent } from './profile/profile.component';
import { PreviousWorkComponent } from './previous-work/previous-work.component';
import { PricingComponent } from './pricing/pricing.component';
import { routedComponents } from '../client/client-routing.module';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';


@NgModule({
  declarations: [
    ReservationsComponent,
    DashboardComponent,
    ProfileComponent,
    PreviousWorkComponent,
    PricingComponent,
    HeaderComponent,
    FooterComponent
  ],
  imports: [
    CommonModule,
    VendorRoutingModule
  ],
  
})
export class VendorModule { }
