import { Component, OnInit } from '@angular/core'
import {CompanyService} from '../company.service'
import { ActivatedRoute, Params} from '@angular/router';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-vendor-profile',
  templateUrl: './vendor-profile.component.html',
  styleUrls: ['./vendor-profile.component.css']
})
export class VendorProfileComponent implements OnInit {

  showModal: boolean= false;

  data: any = []
  profile: any = []
  service: any = []
  rating: any 
  previouswork: any = []
  comment: any = []
  responce: any
  id: string

  comments: FormGroup|any;
  validmessage:string
  name: string
  phone: number
  email: string
  date: Date
  message: string
  

  constructor(private company: CompanyService,private activatedRoute:ActivatedRoute,private fb:FormBuilder) {
    this.activatedRoute.queryParams.subscribe(params => {
      const userId = params['id'];
      this.id = userId
    });

    this.company.getSingleCompanyProfile(this.id).subscribe(data =>{

      this.data =data

      if(this.data.success){

        this.profile=this.data.profile
        this.previouswork = this.data.previoswork
        this.rating = this.data.rating
        this.comment = this.data.comments
        this.service = this.data.service
      }

      console.log(this.comment)

    })
   }

    onClickSubmit() {
      if(this.comments.valid){
        this.validmessage= "your Comment has been submitted"
        this.company.comment(this.comments.value, this.profile._id)
      }
      //console.log(this.id)
      
   }

   Reservation(){

    

    //this.company.addReservation(data)
    
   }

  ngOnInit(): void {

    this.comments = this.fb.group({
      name: new FormControl('', Validators.required),
      email: new FormControl('', Validators.required),
      message: new FormControl('', Validators.required)
    })
  }

}
