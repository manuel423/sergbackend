import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ClientRoutingModule } from './client-routing.module';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { ContactComponent } from './contact/contact.component';
import { ServiceComponent } from './service/service.component';
import { AboutComponent } from './about/about.component';
import { VendorProfileComponent } from './vendor-profile/vendor-profile.component';
import { RouterModule } from '@angular/router';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { SuccessRedirectComponent } from './success-redirect/success-redirect.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


const components = [
  VendorProfileComponent,
  HeaderComponent,
  FooterComponent,
  ContactComponent,
  ServiceComponent,
  AboutComponent,
];

@NgModule({
  declarations: [
   
   LandingPageComponent,...components, SuccessRedirectComponent
  ],
  imports: [CommonModule, ClientRoutingModule, RouterModule, FormsModule, ReactiveFormsModule],
})
export class ClientModule {}
