import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class CompanyService {

  name: string
  email: string
  subject: string
  message: string

  constructor(private http:HttpClient) { }

  getCompanyProfile(){

    let url = "http://localhost:5000/api/vendor"

    return this.http.get(url)
  }


  getSingleCompanyProfile(id: any){
    let url = "http://localhost:5000/api/vendor/"+id
    console.log(url)

    return this.http.get(url)

  }

  addReservation(data: any){
    
      let url = "http://localhost:3000/addReservation"

      return this.http.post(url, {
        data
  
      }
      ).toPromise().then((data: any) =>{
        console.log(data)
      })
  }

  comment(data: any, id: any){
    let url = "http://localhost:5000/api/user/postcomment"
   // var body= JSON.stringify(data)

    return this.http.post(url, {data: data, id: id})
    .toPromise().then((data: any)=>{
      console.log(data)
    })
  }

  
}
