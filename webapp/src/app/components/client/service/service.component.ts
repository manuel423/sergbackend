import { Component } from '@angular/core';
import {CompanyService} from '../company.service'

@Component({
  selector: 'serg-service',
  templateUrl: 'service.component.html',
})
export class ServiceComponent {

  data: any = []

  constructor(private company:CompanyService){

    this.company.getCompanyProfile().subscribe(data =>{
      this.data = data
      console.log(this.data)
    })
  }

  filter(value: any){
    this.data = this.data.filter(s => s.service.includes(value));
  }
}
