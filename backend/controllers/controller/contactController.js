
var contactModel = require('../../models/contactModel.js');

var contacttyeps ={
    ADMIN: "ADMIN",
    VENDOR: "VENDOR",
}

module.exports = {

    list: (req, res) =>{
        contactModel.find(function (err, contacts) {
            if (err) {
                return res.status(200).json({
                    success: false,
                    message: 'Error when getting contact.',
                });
            }
            return res.json(contacts);
        });
    },

    vendors: (req, res) =>{
        contactModel.find( {type: "VENDOR"}, (err, contacts) =>{
            if (err) {
                return res.status(200).json({
                    message: 'Error when getting contact.',
                    error: err
                });
            }
            return res.json(contacts);
        });
    },

    show: (req, res) =>{
        var id = req.params.id;
        contactModel.findOne({_id: id}, function (err, contact) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting contact.',
                    error: err
                });
            }
            if (!contact) {
                return res.status(200).json({
                    success: false,
                    message: 'No such contact'
                });
            }
            return res.json(contact);
        });
    },

    create: (req, res) =>{
        var ty = req.body.type
        var _contacts = new contactModel({
			name : req.body.name,
			email : req.body.email,
			message : req.body.message,
            type: contacttyeps[ty],
            status: "Not replied"
        })

        _contacts.save(function (err, contact) {
            if (err) {
                return res.status(200).json({
                    message: 'Error when creating contact',
                    error: err
                })
            }
            return res.status(200).json({
                success: true,
                message: "Contact Send!"
            })
        })
    },

    remove: (req, res) =>{
        var id = req.params.id;
        contactModel.findByIdAndRemove(id, function (err, contact) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when deleting the contact.',
                    error: err
                });
            }
            return res.status(204).json();
        });
    }
};
