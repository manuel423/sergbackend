
var adminModel = require('../../models/adminModel.js');
var contactModel = require('../../models/contactModel');
var vendorModel = require('../../models/vendorsModel');
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const passgen = require('generate-password')
const nodemailer = require('nodemailer')
const hbs = require('handlebars')
const path = require('path');
const fs = require('fs');
const ratingModel = require('../../models/ratingModel.js');

//hash password
const hashPasword = async (password) => {
    return new Promise((resolve, reject) => {
      bcrypt.genSalt(10, function (err, salt) {
        if (err) {
          return done(err);
        }
        bcrypt.hash(password, salt, (err, hash) => {
          resolve(hash);
        });
      });
    });
  };
  //Password Checker
  const isPasswordCorrect = async (pass1, pass2) => {
    return new Promise((resolve, reject) => {
      bcrypt.compare(pass1, pass2, (err, result) => {
        resolve(result);
      });
    });
  };

  const PB_email = process.env.SENDER_GMAIL_ADDRESS
  const pass = process.env.SENDER_GMAIL_PASSWORD
  
  const email_info = nodemailer.createTransport({
      service: 'gmail',
      auth: {
          user: PB_email,
          pass: pass
      },
      tls: {
          rejectUnauthorized: false
      }
  })
  
  const readHTMLFile = function (path, callback) {
      fs.readFile(path, { encoding: 'utf-8' }, function (err, html) {
          if (err) {
              throw err;
              callback(err);
          }
          else {
              callback(null, html);
          }
      });
  };
  
  const filePath = path.join(__dirname, '../../email/index.html');

module.exports = {

    list: function (req, res) {
        vendorModel.find(function (err, vendors) {
            if (err) {
                return res.status(200).json({
                    success: false,
                    message: 'Error when getting contact.',
                });
            }
            return res.json(vendors);
        });
    },

    vendors: function (req, res) {
        contactModel.find(function (err, contacts) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting contact.',
                    error: err
                });
            }
            return res.json(contacts);
        });
    },

    show: function (req, res) {
        var id = req.params.id;
        contactModel.findOne({_id: id}, function (err, contact) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting contact.',
                    error: err
                });
            }
            if (!contact) {
                return res.status(200).json({
                    success: false,
                    message: 'No such contact'
                });
            }
            return res.json(contact);
        });
    },

    register: async function (req, res) {
        //console.log(req.body);
        if (req.body.reg_key === process.env.ADMIN_REG_KEY) {
          let admin_check = await adminModel.findOne({ username: req.body.username });
          if (admin_check) {
            return res.status(200).json({
              success: false,
              message: `Username already in use`,
            });
          }
          const hashed_password = await hashPasword(req.body.password);
          //console.log("Hashed!!");
          var Admin = new adminModel({
            username: req.body.username,
            password: hashed_password,
          });
    
          Admin.save(function (err, admin) {
            if (err) {
              return res.status(200).json({
                success: false,
                message: "Error when creating User",
                error: err,
              });
            }
            return res.status(200).json({
              success: true,
              message: `Successfully Registered Admin!`,
            });
          });
        }
        else {
          return res.status(200).json({
            success: false,
            message: `No API Route!`,
          });
        }
    
      },

      login: async function (req, res) {
        //console.log("Logging In...");s
        const body = req.body;
        let admin = await adminModel.findOne({ username: body.username });
        //console.log(admin);
        if (!admin) {
          return res.status(200).json({
            success: false,
            message: `User Not found!`,
          });
        } else {
          //console.log(await isPasswordCorrect(body.password, admin.password));
          if (await isPasswordCorrect(body.password, admin.password)) {
            var tmp_admin_obj = {
              _id: admin._id,
              username: admin.username,
              createdAt: admin.createdAt,
              updatedAt: admin.updatedAt,
              __v: admin.__v,
            };
            jwt.sign(
              {
                permissions: ["admin"],
                user: tmp_admin_obj,
              },
              process.env.JWT_SECRET,
              (err, token) => {
                return res.status(200).json({
                  success: true,
                  message: `Sign In Successfull`,
                  data: "Bearer " + token,
                });
              }
            );
          } else {
            return res.status(200).json({
              success: false,
              message: `Incorrect Credential!`,
            });
          }
        }
      },

      addvendor: async function (req, res) {
        //console.log(req.body);
        var data = req.body
        var company_name = data.company_name
        var email = data.email
        var service_type = data.service_type
        var phone = data.phone
        var address = data.address
        var description = data.description
        var slogan = data.slogan

          let vendor_check = await vendorModel.findOne({ email: email });
          if (vendor_check) {
            return res.status(400).json({
              success: false,
              message: `Username already in use`,
            });
          }
          const password = passgen.generate({
            length: 8,
            numbers: true
        })
          const hashed_password = await hashPasword(password);
          //console.log("Hashed!!");
          var vendor = new vendorModel({
            company_name: company_name,
            email: email,
            phone_no: phone,
            service_type: service_type,
            password: hashed_password,
            address: address,
            description: description,
            slogan: slogan
          });

          

          try {
            readHTMLFile(filePath, function (err, html) {
                //console.log(err)

                const template = hbs.compile(html);
                const replacements = {
                    username: vendor.email,
                    password: password,
                    reg_link:process.env.PUBLIC_URL
                };
                const htmlToSend = template(replacements);
                const email = {
                    from: PB_email,
                    to: vendor.email,
                    subject: 'Purpose Black ETH Member Info',
                    html: htmlToSend
                }
                email_info.sendMail(email, (error, info) => {
                    if (error) {
                        //console.log("email not send" + error)
                        return res.status(200).json({
                          success: false,
                          message: "Check your Internet, Email did't send!"
                        })
                    } else {
                        //console.log("email sent" + info)
                        vendor.save(function (err, vendor) {
                          if (err) {
                            return res.status(500).json({
                              success: false,
                              message: "Error when creating User",
                              error: err,
                            });
                          }

                          var rating = new ratingModel({
                            one: 0,
                            two: 0,
                            three: 0,
                            four: 0,
                            five: 0,
                            vendor: vendor.id
                          })

                          rating.save((err, rating)=>{
                            if(err){
                              return res.status(200).json({
                                success: false,
                                message: "Something went wrong!"
                              })
                            }

                            return res.status(200).json({
                              success: true,
                              message: `Successfully Registered vendor!`,
                            });
                          })
                        });
                    }
                })

            })
            
        } catch (error) {
            return res.json({
                success: false,
                message: "email not sent"
            })
        }
    
      },

      deletevendor: function (req, res) {
        var id = req.params.id;
        vendorModel.findByIdAndRemove(id, function (err, contact) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when deleting the contact.',
                    error: err
                });
            }
            return res.status(204).json();
        });
    }
};
