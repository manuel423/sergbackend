var reservationModel = require("../../models/reservationModel.js");
const axios = require("axios");
const path = require('path');
const fs = require('fs')
const nodemailer = require('nodemailer')
const hbs = require('handlebars')
const moment = require('moment')
const { encrypt, decrypt} = require('@rlvt/crypt')


const storeId = process.env.EBIRR_STOREID
const hppKey = process.env.EBIRR_HPPKEY
const merchatId = process.env.EBIRR_MERCHANTUID
const subscriptionId = ""
const ebirr = process.env.EBIRR_EBIRRURL
const successURL = process.env.EBIRR_API_SUCCESS_URL
const errorURL = process.env.EBIRR_API_ERROR_URL    

const RECIEVER_NAME = "PURPOSE BLACK ETH"
const SUBJECT = "Purpose Black ETH Share Subscription"

function getTimestamp() {
    const pad = (n, s = 2) => (`${new Array(s).fill(0)}${n}`).slice(-s);
    const d = new Date();
    return `${pad(d.getFullYear(), 4)}-${pad(d.getMonth() + 1)}-${pad(d.getDate())} ${pad(d.getHours())}:${pad(d.getMinutes())}:${pad(d.getSeconds())}`;
}

function generate(length) {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() *
            charactersLength));
    }
    return result;
}



module.exports = {
    payment_request: async (req, res) =>{
        console.log("e-birr")
        const data = req.body
        const currency = "ETB"
        const invoiceId = await generate(12)
        const referenceId = await generate(12)
        const description = "Purpose Black ETH Share Subscription";
        const price_percentage = data.price
        const name = data.name
        const service = data.service
        const vendor = data.vendor_id
        const email = data.email
        const phone = data.phone
        const price = data.price
        const date = data.date
        const code = await generate(4)

        var reservatin_order = new reservationModel({
            name: name,
            service: service,
            email: email,
            phone: phone,
            price: price,
            date: date,
            code: code,
            vendor: vendor
        })
        
        await reservatin_order.save(async function (err, payment_order) {
            if (err) {
                return res.status(404).json({
                    success: false,
                    message: "Error Saving Payment Order",
                })
            }
            
            var post_data = {
                amount: price,
                currency: currency, 
                invoiceId: invoiceId,
                referenceId: referenceId,
                description: description, 
                storeId: storeId,
                hppKey: hppKey,
                merchatId: merchatId,
                subscriptionId: subscriptionId,
                ebirr: ebirr,
                successURL: successURL,
                errorURL: errorURL
            }

           await axios.post(process.env.EBIRR_API_ADDRESS + "/api/v1/e-birr",{

                post_data
            }).then((responce)=>{
                
                console.log(responce.data)
                return res.status(200).json({
                    success: true,
                    message: "Checkout URL recived!",
                    data: responce.data.resData.params
                })
                
            }).catch(e=>{
                console.log(e)
            })
        });
    


    },

    payment_callback: async (req,res) => {

        //console.log(req.query.hppResultToken)

        let _resToken = req.query.hppResultToken;
        console.log(_resToken);
        var post_data = {
            token: _resToken,
            storeId: storeId,
            hppKey: hppKey,
            merchatId: merchatId,
            ebirr: ebirr,
        }
        axios.post(process.env.EBIRR_API_ADDRESS + "/api/v1/e-birr_callback", {
            post_data
        }).then(async responce =>{
            
            var info = responce.data.data.params
            var timestamp = responce.data.data.timestamp

            if(info.state){

                user_info = {
                    referenceId: info.referenceId,
                    amount: info.txAmount,
                    state: info.state
                }
                let data = encrypt(JSON.stringify(user_info), PASSWORD)

                var payment_order = await payment_orderModel.findOne({ bill_ref_number: info.referenceId, completed: false, payment_method: "e-birr", })
                if (payment_order) {
                    var members_pledge = await pledgeModel.findOne({ member: payment_order.member, payment_complete: false });
                    var payment = await paymentsModel.findOne({ member: payment_order.member, pledge: members_pledge._id });

                    if (!payment) {
                        return res.status(404).json({
                            success: false,
                            message: "No such Payment",
                        });
                    }
                    if (!payment.payment_started && !members_pledge.paid) {
                        payment.payment_started = true;
                        payment.is_platinium_paid = true;
                        payment.paid_amount_in_etb = payment_order.amount_to_be_paid + "";
                        payment.paid_percentage = (payment_order.amount_to_be_paid * 100) / payment.pledged_amount - 5 + "%";
                    }
                    else {
                        payment.payment_started = true;
                        payment.paid_amount_in_etb = parseFloat(payment.paid_amount_in_etb) + parseFloat(payment_order.amount_to_be_paid) + "";
                        payment.paid_percentage =
                            parseFloat(payment.paid_percentage) +
                            (payment_order.amount_to_be_paid * 100) / payment.pledged_amount +
                            "%";
                        payment.payment_started = true;
                    }
                    var member = await memberModel.findOne({ _id: payment_order.member, });
                    if (!members_pledge) {
                        return res.status(404).json({
                            success: false,
                            message: "No such Pledge",
                        });
                    }
                    members_pledge.paid = true;
                    payment_order.completed = true;
                    if(payment.paid_percentage==="100%"){
                        members_pledge.payment_complete = true
                    }
                    var payment_history = new payment_historyModel({
                        transaction_type: "",
                        transaction_id: info.transactionId,
                        transaction_time: timestamp,
                        paid_amount: payment_order.amount_to_be_paid + "",
                        payment: payment._id,
                        payment_order: payment_order._id,
                        phone_number: "",
                        payment_method: "e-birr",
                        bill_ref_number: info.referenceId,
                        status: "confirmed",
                    });
                    payment_history.save(async (err, payment_history)=>{

                        if(err){
                            return res.status(200).json({
                                success: false,
                                message: "Something went wrong!"
                            })
                        }

                        try {

                            const PB_email = process.env.SENDER_GMAIL_ADDRESS;
                            const pass = process.env.SENDER_GMAIL_PASSWORD;
                            const email_info = nodemailer.createTransport({
                                service: "gmail",
                                auth: {
                                    user: PB_email,
                                    pass: pass,
                                },
                                tls: {
                                    rejectUnauthorized: false,
                                },
                            });
                            const readHTMLFile = function (path, callback) {
                                fs.readFile(path, { encoding: "utf-8" }, function (err, html) {
                                    if (err) {
                                        throw err;
                                        callback(err);
                                    } else {
                                        callback(null, html);
                                    }
                                });
                            };
                            
                            const filePath = path.join(__dirname, "../../email/paymentConfirmation.html")

                            try {
                                readHTMLFile(filePath, function (err, html) {
                                    console.log(err);

                                    const template = hbs.compile(html);

                                    const replacements = {
                                        date: moment(payment_history.createdAt).format("MM/DD/YYYY"),
                                        refid: payment_history.transaction_id,
                                        amount: payment_history.paid_amount,
                                        name: member.first_name + " " + member.last_name,
                                        email: member.email,
                                        phone: member.phone,
                                        address: member.address,
                                    };
                                    const htmlToSend = template(replacements);
                                    const email = {
                                        from: PB_email,
                                        to: member.email,
                                        subject: "Purpose Black ETH Member Payment Confirmation",
                                        html: htmlToSend,
                                    };
                                    email_info.sendMail(email, (error, info) => {
                                        if (error) {
                                            console.log("email not send" + error);
                                        } else {
                                            console.log("email sent" + info);
                                        }
                                    });
                                });
                                
                            } catch (error) {
                                return res.json({
                                    success: false,
                                    message: "email not found!"
                                })
                            }
                        }
                        catch (exp) {
                            console.log(exp)
                        }
                        
                        let url = `${process.env.PUBLIC_URL}/stock/payment/e-birr/${data}`
                        return res.redirect(url)
                    });
                   
                }
                else{
                    return res.status(200).json({
                        success: false,
                        message: "Payment Not Recieved",
                    })  
                }

            }
           
        }).catch(e =>{
            console.log(e)
        })
    },

    payment_error: async (req, res)=>{

        let _resToken = req.query.hppResultToken;
        var post_data = {
            token: _resToken,
            storeId: storeId,
            hppKey: hppKey,
            merchatId: merchatId,
            ebirr: ebirr,
        }
        axios.post(process.env.EBIRR_API_ADDRESS + "/api/v1/e-birr_callback", {
            post_data
        }).then(responce =>{
            var info = responce.data.data.params

            user_info = {
                message: "Somethin went wrong!",
                state: info.state
            }
            let data = encrypt(JSON.stringify(user_info), PASSWORD)
             
            let url = `${process.env.PUBLIC_URL}/stock/payment/e-birr/${data}`
            return res.redirect(url)

        }).catch(e =>{
            let error_url = `${process.env.PUBLIC_URL}/error`
            return res.redirect(error_url)
        })

    }
  
}