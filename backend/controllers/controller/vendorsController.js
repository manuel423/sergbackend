const contactModel = require('../../models/contactModel');
const vendorModel = require('../../models/vendorsModel');
const bcrypt = require("bcryptjs");
const ted = require("../../validators/tokenencryptdecrypt.js");
const jwt = require("jsonwebtoken");
const forgotpasswordModel = require('../../models/forgotpasswordModel');
const crypto = require("crypto");
const nodemailer = require('nodemailer')
const hbs = require('handlebars')
const path = require('path');
const fs = require('fs');
const serviceModel = require('../../models/serviceModel');
const previosworkModel = require('../../models/previosworkModel');
const ratingModel = require('../../models/ratingModel');
const commentModel = require('../../models/commentModel');


//hash password
const hashPasword = async (password) => {
    return new Promise((resolve, reject) => {
      bcrypt.genSalt(10, function (err, salt) {
        if (err) {
          return done(err);
        }
        bcrypt.hash(password, salt, (err, hash) => {
          resolve(hash);
        });
      });
    });
  };
  //Password Checker
  const isPasswordCorrect = async (pass1, pass2) => {
    return new Promise((resolve, reject) => {
      bcrypt.compare(pass1, pass2, (err, result) => {
        resolve(result);
      });
    });
  };

const PB_email = process.env.SENDER_GMAIL_ADDRESS
const pass = process.env.SENDER_GMAIL_PASSWORD

const email_info = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: PB_email,
        pass: pass
    },
    tls: {
        rejectUnauthorized: false
    }
})


const readHTMLFile = function (path, callback) {
    fs.readFile(path, { encoding: 'utf-8' }, function (err, html) {
        if (err) {
            throw err;
            callback(err);
        }
        else {
            callback(null, html);
        }
    });
};

const filePath = path.join(__dirname, '../../email/passwordreset.html');

module.exports = {

      list: (req, res) =>{
        vendorModel.find(function (err, vendors) {
            if (err) {
                return res.status(200).json({
                    success: false,
                    message: 'Error when getting contact.',
                });
            }
            return res.json(vendors);
        });
    },

    getContact: (req, res) =>{
        contactModel.find(function (err, vendors) {
            if (err) {
                return res.status(200).json({
                    success: false,
                    message: 'Error when getting contact.',
                });
            }
            return res.json(vendors);
        });
    },

    getcompanyProfile:async (req, res) =>{
      var id = req.params.id
      let data = await vendorModel.findOne({id: id})

      if(!data){
        return res.status(200).json({
          success: false,
          message: "Somthing went Wrong"
        })
      }

      let service  = await serviceModel.find({vendor: id})
      let previouswork = await previosworkModel.find({vendor: id})
      let all_rating = await ratingModel.findOne({vendor: id})
      let comments = await commentModel.find({vendor: id})
      var rating =0
      console.log(all_rating)

      if(all_rating.one === 0 && all_rating.two === 0 && all_rating.three === 0 && all_rating.four === 0 && all_rating.five === 0  ){
        rating = 0
      }else{
        rating = (5*all_rating.five + 4*all_rating.four + 3*all_rating.three + 2*all_rating.two + 1*all_rating.one) / (all_rating.five+all_rating.four+all_rating.three+all_rating.two+all_rating.one)
      }

      return res.status(200).json({
        success: true,
        profile: data,
        service: service,
        previoswork: previouswork,
        rating: rating,
        comments: comments
      })
      
    },

      login: async (req, res) =>{
        //console.log("Logging In...");s
        const data = req.body;
        let vendor = await vendorModel.findOne({ email: data.email });
        //console.log(vendor);
        if (!vendor) {
          return res.status(200).json({
            success: false,
            message: `Username Not Found!`,
          });
        } else {
          //console.log(await isPasswordCorrect(data.password, vendor.password));
          if (await isPasswordCorrect(data.password, vendor.password)) {
            var tmp_vendor_obj = {
              _id: vendor._id,
              username: vendor.username,
              createdAt: vendor.createdAt,
              updatedAt: vendor.updatedAt,
              __v: vendor.__v,
            };
            jwt.sign(
              {
                permissions: ["vendor"],
                user: tmp_vendor_obj,
              },
              process.env.JWT_SECRET,
              (err, token) => {
                return res.status(200).json({
                  success: true,
                  message: `Sign In Successfull`,
                  data: "Bearer " + token,
                });
              }
            );
          } else {
            return res.status(200).json({
              success: false,
              message: `Incorrect Credential!`,
            });
          }
        }
      },

    remove: (req, res) =>{
        var id = req.params.id;
        vendorModel.findByIdAndRemove(id, function (err, contact) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when deleting the contact.',
                    error: err
                });
            }
            return res.status(200).json({
              success: true,
              message: "vendor deleted!"
            });
        });
    },

    addprofile: (req, res) =>{
      //console.log(req.files.logo[0].filename)
        // var data = req.filePath

        // console.log(data)

    },
    updateprofile:async (req, res) =>{
        var data = req.body
        var files = req.files
        var id = "6147d70740adea69bb44df39"
        var profile = await vendorModel.findOne({_id: id})

        profile.company_name = data.company_name ? data.company_name : profile.company_name
        profile.slogan = data.company_name ? data.slogan : profile.slogan
        profile.description = data.company_name ? data.description : profile.description
        profile.email = data.company_name ? data.email : profile.email
        profile.phone_no = data.company_name ? data.phone_no : profile.phone_no
        profile.address = data.company_name ? data.address : profile.address
        profile.logo = files.logo[0].filename ? files.logo[0].filename : ""
        profile.covere_pic = files.pic[0].filename ? files.pic[0].filename : ""

        profile.save((err, profile)=>{
          if(err){
            console.log(err)
            return res.status(200).json({
              success: false,
              message: "Something went wrong!"
            })
          }

          return res.status(200).json({
            success: true,
            message: "Profile Updated successfuly!"
          })
        })
        

    },

    getreservation: (req, res) =>{
        var data = req.body

    },

    addwork: (req, res) =>{

    },

    updatePassword: async (req, res) =>{
      const data = req.body;
      //console.log(data);
      //console.log(req.body.id);
      const id = req.body.id;
      let vendor = await vendorModel.findOne({ _id: id });
      //console.log(vendor);
  
      if (!vendor) {
        return res.status(500).json({
          message: "No such vendor",
          success: false,
        });
      } else {

      const hashed_password = await hashPasword(data.new_password);

        if (await isPasswordCorrect(data.old_password, vendor.password)) {
          vendor.password = hashed_password ? hashed_password : vendor.password;
  
          vendor.save( (err, vendor) =>{
            if (err) {
              return res.status(200).json({
                message: "Error when updating password.",
                error: err,
                success: false,
              });
            } else {
              return res.status(200).json({
                success: true,
                message: "Password Updated",
              });
            }
          });
        } else {
          return res.status(200).json({
            success: false,
            message: "incorrect Password",
          });
        }
      }
    },

    forgotpassword: async function (req, res) {
        const email = req.body.email.toLowerCase();
        //console.log(email);
    
        let vendor = await vendorModel.findOne({ email: email });
    
        if (!vendor) {
          return res.status(200).json({
            success: false,
            message: "Email not found!",
          });
        }
    
        const data = {
          email: email,
          id: vendor._id,
        };
    
        let forgotpassword_check = await forgotpasswordModel.findOne({
          $and: [{ vendor: vendor.id }, { done: false }],
        });
    
        //console.log(forgotpassword_check);
    
        if (!forgotpassword_check) {
          const random_key = crypto.randomBytes(32).toString("hex");
          const key = random_key + vendor.password;
          const hashed_key = crypto.createHash("sha256").update(key).digest("hex");
    
          const token = jwt.sign(data, hashed_key, { expiresIn: "30m" });
          const encryptedToken = ted.encrypt(token);
    
          var forgotpassword = new forgotpasswordModel({
            email: email,
            token: hashed_key,
            done: false,
            vendor: vendor._id,
          });
    
          try {
            readHTMLFile(filePath, function (err, html) {
              ////console.log(err)
              const template = hbs.compile(html);
              const replacements = {
                link: `${process.env.PUBLIC_URL}/user/reset-password?id=${vendor._id}&token=${encryptedToken}`,
                name: vendor.first_name,
              };
              const htmlToSend = template(replacements);
              const emails = {
                from: PB_email,
                cc: "bestaman.85@gmail.com",
                to: email,
                subject: "PurposeBlack ETH vendor password reset",
                html: htmlToSend,
              };
              email_info.sendMail(emails, (error, info) => {
                if (error) {
                  //console.log("email not send" + error);
                } else {
                  forgotpassword.save(function (err, vendor) {
                    if (err) {
                      return res.status(500).json({
                        message: "Error when updating password.",
                        error: err,
                      });
                    }
                    //console.log("email sent", info);
    
                    return res.status(200).json({
                      success: true,
                      message: "email sent!",
                    });
                  });
                }
              });
            });
          } catch (error) {
            return res.json({
              success: false,
              message: "email not sent!",
            });
          }
        } else {
          //console.log("somthing");
          forgotpassword_check.done = true;
          forgotpassword_check.save((err, forgot) => {
            const random_key = crypto.randomBytes(32).toString("hex");
            const key = random_key + vendor.password;
            const hashed_key = crypto
              .createHash("sha256")
              .update(key)
              .digest("hex");
    
            const token = jwt.sign(data, hashed_key, { expiresIn: "30m" });
            const encryptedToken = ted.encrypt(token);
    
            var forgotpassword = new forgotpasswordModel({
              email: email,
              token: hashed_key,
              done: false,
              vendor: vendor._id,
            });
    
            try {
              readHTMLFile(filePath, function (err, html) {
                ////console.log(err)
                const template = hbs.compile(html);
                const replacements = {
                  link: `${process.env.PUBLIC_URL}/user/reset-password?id=${vendor._id}&token=${encryptedToken}`,
                  name: vendor.full_name,
                };
                const htmlToSend = template(replacements);
                const emails = {
                  from: PB_email,
                  cc: "bestaman.85@gmail.com",
                  to: email,
                  subject: "Serg vendor account password reset",
                  html: htmlToSend,
                };
                email_info.sendMail(emails, (error, info) => {
                  if (error) {
                    //console.log("email not send" + error);
                  } else {
                    forgotpassword.save(function (err, vendor) {
                      if (err) {
                        return res.status(500).json({
                          message: "Error when updating password.",
                          error: err,
                        });
                      }
                      console.log( info);
    
                      return res.status(200).json({
                        success: true,
                        message: "email sent!",
                      });
                    });
                  }
                });
              });
            } catch (error) {
              return res.json({
                success: false,
                message: "email not sent!",
              });
            }
          });
        }
      },

      verifypasswordresettoken: async function (req, res) {
        const id = req.body.id;
        const token = req.body.token;
        //console.log(id);
        let vendor = await vendorModel.findOne({ _id: id });
        if (!vendor) {
          return res.status(404).json({
            success: false,
            message: "vendor not found!",
          });
        }
        let forgotpassword = await forgotpasswordModel.findOne({
          $and: [{ vendor: id }, { done: false }],
        });
        if (forgotpassword) {
          try {
            const decryptedToken = ted.decrypt(token);
            const verifyToken = jwt.verify(decryptedToken, forgotpassword.token);
            return res.status(200).json({
              success: true,
              message: "token verifyed!",
            });
          } catch (err) {
            forgotpassword.token = "Invalid token used or link expierd";
            forgotpassword.done = true;
            forgotpassword.save((err, forgotpassword) => {
              if (err) {
                //console.log(err);
                return res.status(500).json({
                  success: false,
                  message: "Something went wrong!",
                });
              }
              //console.log("token verify faild");
              return res.status(303).json({
                success: false,
                message: "invalid token or link expierd",
              });
            });
          }
        } else {
          return res.status(303).json({
            success: false,
            message: "invalid token or link expierd",
          });
        }
      },

      passwordreset: async function (req, res) {
        const id = req.body.id;
        const token = req.body.token;
        const password = req.body.password;
        let vendor = await vendorModel.findOne({ _id: id });
        let forgotpassword = await forgotpasswordModel.findOne({
          $and: [{ vendor: id }, { done: false }],
        });
        if (!vendor) {
          return res.status(404).json({
            success: false,
            message: "vendor not found!",
          });
        }
        const decryptedToken = ted.decrypt(token);
        try {
          const verifyToken = jwt.verify(decryptedToken, forgotpassword.token);
          const hashed_password = await hashPasword(password);
          vendor.password = password ? hashed_password : vendor.password;
          vendor.save(function (err, vendor) {
            if (err) {
              //console.log(err);
              return res.status(500).json({
                success: false,
                message: "Password reset failed! vendor",
              });
            }
            forgotpassword.token = "Password changed";
            forgotpassword.done = true;
            forgotpassword.save((err, forgotpassword) => {
              if (err) {
                //console.log(err);
                return res.status(500).json({
                  success: false,
                  message: "Password reset failed! forgot",
                });
              }
              return res.status(200).json({
                success: true,
                message: "Password reseted!",
              });
            });
          });
        } catch (e) {
          //console.log(e);
          return res.status(500).json({
            success: false,
            message: "token invalid or link expierd",
          });
        }
      },

};
