const adminController = require("./controller/adminController");
const contactController = require("./controller/contactController");
const vendorsController = require("./controller/vendorsController");
const ebirrController = require("./controller/e-birr_online_controller");
const userController = require("./controller/userController")

module.exports = {
  adminController,
  contactController,
  vendorsController,
  ebirrController,
  userController
};
