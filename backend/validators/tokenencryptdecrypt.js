const { encrypt, decrypt } = require('@rlvt/crypt')
const PASSWORD = process.env.SERG_SECRETE_KEY
module.exports = {
  decrypt(text) {
    return decrypt(text,PASSWORD);
  }
  , encrypt(text) {
    return encrypt(text,PASSWORD);
  }
}

