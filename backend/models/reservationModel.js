var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var reservationSchema = new Schema({
	name:{
        type:String,
    },
    email: {
        type: String
    },
    phone: {
        type: String
    },
	service:{
        type:String,
    },
	price:{
        type:Number
    },
    vendor:{
        type:String
    },
    date:{
        type: String
    },
    code: {
        type: String
    },
    complete: {
        type: Boolean,
        default: false
    }

});

reservationSchema.set('timestamps',true);

module.exports = mongoose.model('reservation', reservationSchema);
