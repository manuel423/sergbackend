var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var adminSchema = new Schema({
  username: {
    type: String,
    unique: true,
  },
  password: String,
});

adminSchema.set("timestamps", true);

module.exports = mongoose.model("admin", adminSchema);
