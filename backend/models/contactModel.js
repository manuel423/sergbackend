var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var contactSchema = new Schema({
	name:{
        type:String,
        required: true
    },
	email:{
        type:String,
        required: true
    },
	message:{
        type:String
    },

    type:{
        type:String
    },

    status:{
        type: String
    }

});

contactSchema.set('timestamps',true);

module.exports = mongoose.model('contactus', contactSchema);
