var mongoose = require('mongoose')
var Schema = mongoose.Schema

var ratingSchema = new Schema({
    one: {
        type: Number,
        default: 0
    },
    
    two: {
        type: Number,
        default: 0 
    },

    three: {
        type: Number,
        default: 0
    },

    four: {
        type: Number,
        default: 0
    },

    five: {
        type: Number,
        default: 0
    },

    vendor:{
        type: Schema.Types.ObjectId,
        ref: 'vendor'
    }
    
})

ratingSchema.set('timestamps', true)

module.exports = mongoose.model('rating', ratingSchema)