var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var commentSchema = new Schema({
	name:{
        type:String,
    },
    email:{
        type:String,
    },
	comment:{
        type:String
    },
    vendor : {
        type: Schema.Types.ObjectId,
        ref: 'vendor'
   }

});

commentSchema.set('timestamps',true);

module.exports = mongoose.model('comment', commentSchema);
