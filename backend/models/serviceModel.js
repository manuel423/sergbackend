var mongoose = require('mongoose')
var Schema = mongoose.Schema

var serviceSchema = new Schema({
    description: String,
    price: Number,
    vendor: {
        type: Schema.Types.ObjectId,
        ref: 'vendor'
    }
})

serviceSchema.set('timestamps', true)

module.exports = mongoose.model('service', serviceSchema)