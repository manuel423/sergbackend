var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var forgotpasswordSchema = new Schema({
	'email' : String,
	'token' : String,
	'done' : Boolean,
	'vendor' : {
	 	type: Schema.Types.ObjectId,
	 	ref: 'vendor'
	}
});

forgotpasswordSchema.set('timestamps',true);

module.exports = mongoose.model('forgotpassword', forgotpasswordSchema);
