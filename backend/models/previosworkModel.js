const mongoose = require('mongoose')
const Schema = mongoose.Schema

var previousworkSchema = new Schema({
    titel:{
        type:String,
        required: true
    },

    link:{
        type:String,
        required: true
    },

    description:{
        type:String,
    },

    vendor:{
        type: Schema.Types.ObjectId,
	 	ref: 'vendor'
    },
})

previousworkSchema.set('timestamps', true)

module.exports = mongoose.model('previouswork', previousworkSchema)

