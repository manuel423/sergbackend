var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var vendorSchema = new Schema({
	company_name:{
        type:String,
        
    },
    phone_no:{
        type:String,
       
    },
	email:{
        type:String,
        
    },
    service_type:{
        type:String,
    },
    slogan: {
        type: String
    },
    address:{
        type:String
    },
    description:{
        type: String
    },
    logo: {
        type: String
    },
    covere_pic: {
        type: String
    },
	password:{
        type:String
    },

});

vendorSchema.set('timestamps',true);

module.exports = mongoose.model('vendor', vendorSchema);