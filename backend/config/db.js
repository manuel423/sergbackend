const mongoose = require("mongoose");
const dotenv = require("dotenv");

dotenv.config();
var db_url = "";
if (process.env.NODE_ENV === "production") {
  db_url = `mongodb://${process.env.DB_URL}:${process.env.MONGODB_PORT}/${process.env.DB_NAME}?retryWrites=true&w=majority`;
}
else{
  db_url = `mongodb://${process.env.DB_URL}:${process.env.MONGODB_PORT}/${process.env.DB_NAME}?retryWrites=true&w=majority`;
}

const connectDb = async () => {
  return await mongoose.connect(db_url, {
    useUnifiedTopology: true,
    useNewUrlParser: true,
  }).catch(err => {
    if (err.message.indexOf("ECONNREFUSED") !== -1) {
      console.error("Error: The server was not able to reach MongoDB. Maybe it's not running?");
      process.exit(1);
  } else {
      throw err;
  }
  });
};
module.exports = { connectDb };
