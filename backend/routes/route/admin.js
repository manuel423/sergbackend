var express = require('express')
var router = express.Router()
const controller = require('../../controllers/index.js')
const authorize = require('../../validators/authorize')

router.get('/getvendors',  authorize.validateToken, controller.adminController.list);
router.post('/', controller.adminController.register);
router.post('/addvendor', controller.adminController.addvendor);
router.post('/login', controller.adminController.login);
router.delete('/deletevendor', authorize.validateToken, controller.adminController.deletevendor);

module.exports = router;
