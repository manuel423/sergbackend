var express = require('express');
var router = express.Router();
const controller = require('../../controllers/index.js');
const authorize = require('../../validators/authorize')
const multer = require('multer')

const storage = multer.diskStorage({
    destination: (request, file, callback) =>{
      callback(null, '../webapp/src/assets/images/uploads')
    },
  
    filename: (request, file, callback)=>{
      callback(null, Date.now()+file.originalname)
    }
  })
  
  const upload = multer({
    storage: storage,
    limits:{
      fileSize: 1024 * 1024 * 3
    }
  })

router.get('/', controller.vendorsController.list)
router.get('/:id', controller.vendorsController.getcompanyProfile)

router.get('/reservation',  authorize.validateToken, controller.vendorsController.getreservation);
router.get('/contacs', authorize.validateToken, controller.vendorsController.getContact)

router.post('/addwork', controller.vendorsController.addwork);
router.post("/login", controller.vendorsController.login);
router.post("/forgot-password", controller.vendorsController.forgotpassword);
router.post("/verifyresetpasswordtoken", controller.vendorsController.verifypasswordresettoken);
router.put('/updateProfile',upload.fields([{
    name: 'logo', maxCount: 1
  }, {
    name: 'pic', maxCount: 1
  }]), controller.vendorsController.updateprofile);

router.put("/changePassword", authorize.validateToken, controller.vendorsController.updatePassword);
router.put("/reset-password", controller.vendorsController.passwordreset);

module.exports = router;
