var express = require('express')
var router = express.Router()
const controller = require('../../controllers/index.js')
const authorize = require('../../validators/authorize')

router.post('/postcomment', controller.userController.comment);
router.post('/reservation', controller.ebirrController.payment_request);
router.post('/rating', controller.userController.rating);


module.exports = router;
