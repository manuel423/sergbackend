var express = require('express');
var router = express.Router();
const controller = require('../../controllers/index.js');
const authorize = require('../../validators/authorize')

router.get('/', controller.contactController.list);
router.get('/getcontacts', controller.contactController.vendors);
router.post('/create', controller.contactController.create);

module.exports = router;
