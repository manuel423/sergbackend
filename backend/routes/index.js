// API ROUTE HANDLERS
const express = require("express");
const router = express.Router()

const admin_routes = require("./route/admin");
const vendor_routes = require("./route/vendors");
const contact_routes = require("./route/contact");
const user_routes = require("./route/user");

router.use("/admin", admin_routes);
router.use("/vendor", vendor_routes);
router.use("/contact", contact_routes);
router.use("/user", user_routes);

module.exports = router;
