require("dotenv").config({ path: __dirname + "/.env" });
const backup_task = require("./services/backup");
const express = require("express");
const connectDb = require("./config/db").connectDb;
const bodyParser = require("body-parser");
const path = require("path");
//const error_handler = require('./validators/error_handler')
const cors = require("cors");
const helmet = require("helmet");
const routeAPI = require("./routes/index.js");


const app = express();

connectDb().then((res)=> console.log("connected")).catch(e => console.log(e));
port = process.env.PORT || 5000;

app.use(function (req, res, next) {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, OPTIONS, PUT, PATCH, DELETE"
  );
  res.setHeader(
    "Access-Control-Allow-Headers",
    "X-Requested-With,content-type,X-Total-Count"
  );
  res.header("Access-Control-Expose-Headers", "X-Total-Count");
  res.setHeader("Access-Control-Allow-Credentials", true);
  // res.setHeader("Content-Security-Policy", "default-src *; style-src 'self' 'unsafe-inline'; font-src 'self' data:; script-src 'self' 'unsafe-inline' 'unsafe-eval' stackexchange.com")
  next();
});
app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(helmet());
app.use("/api", routeAPI);

// app.use("*", (req, res) => {
//   res.json({ message: "no API route" });
// });

app.listen(port, () => console.log(`App listening on port ${port}!`));

//backup_task.start();
//update_currency_rate.start()
